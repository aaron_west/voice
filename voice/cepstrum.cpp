#include <cmath>
#include <string>
#include <iomanip>
#include <stdexcept>


#include "cepstrum.h"
#include "signalproc.h"

using namespace std;



//  ##     ## ######## ##        ######   ######  ##
//  ###   ### ##       ##       ##    ## ##    ## ##
//  #### #### ##       ##       ##       ##       ##
//  ## ### ## ######   ##        ######  ##       ##
//  ##     ## ##       ##             ## ##       ##
//  ##     ## ##       ##       ##    ## ##    ## ##
//  ##     ## ######## ########  ######   ######  ########

double __melscconv::tomel(double value, __melscapprox approximation) {
    double converted = 0.;
    switch (approximation) {
        case __melscapprox::natural:
            converted = 1127. * log (1. + value/700.);
            break;
        case __melscapprox::decimal:
            converted = 2595. * log10 (1. + value/700.);
            break;
        default:
            throw out_of_range("CPST: "
            "Invalid mel-scale approximation specified.");
    }
    return converted;
}

double __melscconv::frommel(double value, __melscapprox approximation) {
    double converted = 0.;
    switch (approximation) {
        case __melscapprox::natural:
            converted = 700. * (exp (value/1127.) - 1.);
            break;
        case __melscapprox::decimal:
            converted = 700. * (pow (10., value/2595.) - 1.);
            break;
        default:
            throw out_of_range("CPST: "
            "Invalid mel-scale approximation specified.");
    }
    return converted;
}



//   ######  ######## ########   ######  ######## ########
//  ##    ## ##       ##     ## ##    ##    ##    ##     ##
//  ##       ##       ##     ## ##          ##    ##     ##
//  ##       ######   ########   ######     ##    ########
//  ##       ##       ##              ##    ##    ##   ##
//  ##    ## ##       ##        ##    ##    ##    ##    ##
//   ######  ######## ##         ######     ##    ##     ##

mfcc20::mfcc20() {
    _high = (_low = 0.);
    _size = (_samplfreq = NAN);
    _boundaries = vector<double>(0);
    return;
}

void mfcc20::setlow(double frequency) {
    if (frequency == NAN)
        throw invalid_argument("CPST: "
        "Low frequency parameter is invalid.");
    _low = frequency;
    return;
}
void mfcc20::sethigh(double frequency) {
    if (frequency == NAN)
        throw invalid_argument("CPST: "
        "High frequency parameter is invalid.");
    _high = frequency;
    return;
}

void mfcc20::_calcboundaries() {
    if (_size == NAN || _samplfreq == NAN)
        throw invalid_argument("CPST: "
        "mfccfb20 instance is not initialised");
    _boundaries = vector<double>(_filters + 2);
    double __temporary = 0.;
    for (size_t __counter = 0;
         __counter < _boundaries.size();
         __counter++) {
        __temporary = __melscconv::tomel(_high, natural);
        __temporary -= __melscconv::tomel(_low, natural);
        __temporary *= (__counter / (_filters + 1.));
        __temporary += __melscconv::tomel(_low, natural);
        __temporary = __melscconv::frommel(__temporary, natural);
        __temporary *= (static_cast<double>(_size) /
                      static_cast<double>(_samplfreq));
        _boundaries[__counter] = __temporary;
    }
    return;
}

vector<double> mfcc20::_filter(size_t filter,
                               vector<double> *data) {
    if (data == nullptr)
        throw invalid_argument("CPST: "
        "Pointer provided is invalid.");
    if (data->empty() || (data->size() == 0))
        throw logic_error("CPST: "
        "The data container provided is empty.");
    if (filter == NAN)
        throw invalid_argument("CPST: "
        "Filter number parameter is invalid.");
    if (filter > _boundaries.size() - 2)
        throw out_of_range("CPST: "
        "Filter number parameter is out of range.");
    auto __processed = vector<double>((*data).size());
    double __temporary = 0.;
    for (size_t __counter = 0;
         __counter < (*data).size();
         __counter++) {
        if (__counter < _boundaries[filter])
            __temporary = 0.;
        else if (__counter < _boundaries[filter + 1]) {
            __temporary = (__counter - _boundaries[filter]);
            __temporary /= (_boundaries[filter + 1] -
                          _boundaries[filter]);
        }
        else if (__counter < _boundaries[filter + 2]) {
            __temporary = (_boundaries[filter + 2] - __counter);
            __temporary /= (_boundaries[filter + 2] -
                          _boundaries[filter + 1]);
        }
        else __temporary = 0.;
        __processed[__counter] = (*data)[__counter] * __temporary;
    }
    return __processed;
}

double mfcc20::_calcenergy(vector<double> *data) {
    if (data == nullptr)
        throw invalid_argument("CPST: "
        "Pointer provided is invalid.");
    if ((*data).empty() ||
        (*data).size() == 0)
        throw runtime_error("CPST: "
        "The data container provided is empty.");
    double __energy = 0., __temporary = 0.;
    for (size_t __counter = 0;
         __counter < (*data).size();
         __counter++) {
        __temporary = __energy;
        __energy += (*data)[__counter];
        if (__energy < __temporary)
            throw overflow_error("CPST: "
            "Double type overflowed.");
    }
    if (__energy != 0.)
        __energy = log10(__energy);
    return __energy;
}

vector<double> mfcc20::_getMFCC(vector<double> *energies) {
    if (energies == nullptr)
        throw invalid_argument("CPST: "
        "Pointer provided is invalid.");
    if ((*energies).empty() ||
        (*energies).size() == 0)
        throw logic_error("CPST: "
        "The data container provided is empty.");
    double __current = 0., __argument = 0.;
    vector<double> MFCC(_coefs);
    for (size_t __coef = 1;
         __coef <= _coefs;
         __coef++) {
        __current = 0.;
        for (size_t __counter = 0;
             __counter < (*energies).size();
             __counter++) {
            __argument = __coef * (__counter - 0.5) * pi;
            __argument /= (*energies).size();
            __argument = cos(__argument);
            __current += (*energies)[__counter] * __argument;
        }
        MFCC[__coef - 1] = __current;
    }
    return MFCC;
}

void mfcc20::init(unsigned int fftsize, unsigned int samplfreq) {
    if (fftsize == NAN)
        throw invalid_argument("CPST: "
        "FFT size parameter is invalid.");
    if (samplfreq == NAN)
        throw invalid_argument("CPST: "
        "Sampling frequency parameter is invalid.");
    cout << "[Initialising mfcc processor]\n";
    _size = fftsize;
    _samplfreq = samplfreq;
    sethigh(44000.);
    setlow(1.);
    _calcboundaries();
    return;
}

vector<double> mfcc20::process(vector<double> *data) {
    if ((_boundaries.size() == 0) || _boundaries.empty())
        throw logic_error("CPST: "
        "mfccfb20 instance is not initialised");
    if ((*data).empty() || (*data).size() == 0)
        throw logic_error("CPST: "
        "The data container provided is empty.");
    vector<double> __processed((*data).size()), __energies(0);
    for (size_t __filter = 1;
         __filter <= _filters;
         __filter++) {
        __processed = _filter(__filter, data);
        __energies.push_back(_calcenergy(&__processed));
    }
    return _getMFCC(&__energies);
}
