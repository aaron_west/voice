#include "configs.h"

uint32_t configs::_samplingfreq = SAMPLING_FREQUENCY;
uint32_t configs::_fftsize = FFT_SIZE;
uint32_t configs::_dimensions = DIMENSIONS;
string configs::_dbpath = DEFAULT_DBPATH;

uint32_t configs::samplingfreq() {
    return _samplingfreq;
}

void configs::samplingfreq(uint32_t freq) {
    _samplingfreq = freq;
}

uint32_t configs::fftsize() {
    return _fftsize;
}

void configs::fftsize(uint32_t fftsize) {
    _fftsize = fftsize;
}

uint32_t configs::dimensions() {
    return _dimensions;
}

void configs::dimensions(uint32_t dimensions) {
    _dimensions = dimensions;
}

string configs::dbpath() {
    return _dbpath;
}

void configs::dbpath(string dbpath) {
    _dbpath = dbpath;
}
