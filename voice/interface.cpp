#include <iostream>
#include <iomanip>

#include "interface.h"
#include "signalproc.h"

using namespace std;



//   ######   #######  ##    ##  ######  ##
//  ##    ## ##     ## ###   ## ##    ## ##
//  ##       ##     ## ####  ## ##       ##
//  ##       ##     ## ## ## ##  ######  ##
//  ##       ##     ## ##  ####       ## ##
//  ##    ## ##     ## ##   ### ##    ## ##
//   ######   #######  ##    ##  ######  ########

void console::log(string output) {
    cout << output;
    return;
}

void console::showdb(map<string, vector<double>> *db) {
    int __index = 0;
    for (auto __iterator = db->begin();
         __iterator != db->end();
         __iterator++, __index++) {
        cout << "\t[" << __index << "]\t" << __iterator->first << "\n\t\t";
        for (size_t __coef = 0;
             __coef < __iterator->second.size();
             __coef++)
            cout << __iterator->second[__coef] << " ";
        cout << "\n\n";
    }
    if (db->size() == 0) {
        cout << "\tNo users found.\n";
    }
    return;
}



//   ######  ##       ####
//  ##    ## ##        ##
//  ##       ##        ##
//  ##       ##        ##
//  ##       ##        ##
//  ##    ## ##        ##
//   ######  ######## ####

map<string, int> interface::_switches = map<string, int>{
    {"-u", _commands::load},
    {"--load", _commands::load},
    {"-p", _commands::process},
    {"--proc", _commands::process},
    {"-c", _commands::config},
    {"--conf", _commands::config},
    {"--set", _commands::config},
    {"-l", _commands::learn},
    {"--lrn", _commands::learn},
    {"-d", _commands::identify},
    {"--idtf", _commands::identify},
    {"-s", _commands::show},
    {"--show", _commands::show},
    {"-?", _commands::manual},
    {"-h", _commands::manual},
    {"--man", _commands::manual},
    {"-t", _commands::test},
    {"--test", _commands::test},
    {"--db", _commands::database},
    {"-b", _commands::database},
    {"--add", _commands::add},
    {"--rem", _commands::remove},
    {"--all", _commands::all},
    {"-r", _commands::random},
    {"--rnd", _commands::random}
};

map<string, int> interface::_parameternames = map<string, int>{
    {"--smplfreq", _parameters::samplingfreq},
    {"-SF", _parameters::samplingfreq},
    {"--fftsize", _parameters::fftsize},
    {"-FFTS", _parameters::fftsize},
    {"--dimns", _parameters::dimensions},
    {"-D", _parameters::dimensions},
    {"--dfdb", _parameters::dbpath},
    {"-DDBP", _parameters::dbpath}
};


void interface::launch(int argc, const char * argv[]) {
    try {
        cout << "[Application launched]\n";
        if (argc < 2) {
            throw runtime_error("INTR: "
            "Voice application CLI requires parameters. "
            "See manual [switch -?] for help.");
        }
        else
        {
            auto __command = _switches.find(argv[1])->second;
            switch (__command) {
                case (load):
                {
                    if (argc == 3) {
                        string __samplespath = argv[2];
                        if (valid(&__samplespath))
                            logic::load(__samplespath);
                        else throw runtime_error("INTR: "
                        "Invalid samples binary path specified.");
                    } else throw runtime_error("INTR: "
                    "Load command requires one parameter. "
                    "See manual [switch -?] for help.");
                }
                    break;
                case (random):
                {
                    /* DUMB */
                    // This will do the trick
                    if (argc == 4) {
                        string __mfccpath = argv[2];
                        string __userindex = argv[3];
                        if (valid(&__mfccpath))
                            logic::dumbness(__mfccpath,
                                            __userindex);
                        else throw runtime_error("INTR: "
                        "Invalid mfcc binary path specified.");
                    } else throw runtime_error("INTR: "
                    "Rnd command requires three parameters. "
                    "Don't see manual [switch -?] for help.");
                }
                    break;
                case (process):
                {
                    if (argc == 4) {
                        string __samplespath = argv[2];
                        string __mfccpath = argv[3];
                        if (valid(&__samplespath) &&
                            valid(&__mfccpath))
                            logic::process(__samplespath,
                                           __mfccpath);
                        else throw runtime_error("INTR: "
                        "Invalid samples or mfcc binary "
                        "path specified.");
                    } else throw runtime_error("INTR: "
                    "Process command requires two parameters. "
                    "See manual [switch -?] for help.");
                }
                    break;
                case (learn):
                {
                    if (argc == 4) {
                        string __mfccpath = argv[2];
                        string __username = argv[3];
                        if (valid(&__mfccpath))
                            logic::learn(__mfccpath,
                                         __username);
                        else throw runtime_error("INTR: "
                        "Invalid mfcc binary path specified.");
                    } else throw runtime_error("INTR: "
                    "Learn command requires two parameters. "
                    "See manual [switch -?] for help.");
                }
                    break;
                case (identify):
                {
                    if (argc == 3) {
                        string __mfccpath = argv[2];
                        if (valid(&__mfccpath))
                            logic::identify(__mfccpath);
                        else throw runtime_error("INTR: "
                        "Invalid mfcc binary path specified.");
                    } else throw runtime_error("INTR: "
                    "Identify command requires one parameter. "
                    "See manual [switch -?] for help.");
                }
                    break;
                case (database):
                {
                    if (argc == 3) {
                        string __operationstr = argv[2];
                        auto __operation =
                        _switches.find(__operationstr)->second;
                        switch (__operation) {
                            case (show):
                                data::showdb(configs::dbpath());
                                break;
                            case (all):
                                data::showdbfull(configs::dbpath());
                                break;
                            default:
                                throw runtime_error("INTR: "
                                "Invalid CLI command specified. "
                                "See manual [switch -?] for help.");
                                break;
                        }
                    } else if (argc == 4) {
                        string __operationstr = argv[2];
                        string __username = argv[3];
                        auto __operation =
                        _switches.find(__operationstr)->second;
                        switch (__operation) {
                            case (add):
                                data::adduser(configs::dbpath(),
                                              __username);
                                break;
                            case (remove):
                                data::removeuser(configs::dbpath(),
                                                 __username);
                                break;
                            case (show):
                                data::showuser(configs::dbpath(),
                                               __username);
                                break;
                            case (all):
                                data::showallforuser(configs::dbpath(),
                                               __username);
                                break;
                            default:
                                throw runtime_error("INTR: "
                                "Invalid CLI command specified. "
                                "See manual [switch -?] for help.");
                                break;
                        }
                    } else throw runtime_error("INTR: "
                    "Database management commands requires "
                    "one or three parameters. "
                    "See manual [switch -?] for help.");
                }
                    break;
                case (manual):
                    man();
                    break;
                /* FOR FUTURE VERSIONS */
                // add config here
                default:
                    throw runtime_error("INTR: "
                    "Invalid CLI command specified. "
                    "See manual [switch -?] for help.");
                    break;
            }
        }
    } catch (exception &exc) {
        console::log((string)exc.what() + "\n");
    }
    return;
}

void interface::man() {
    console::log("============================== APPLICATION MANUAL ==============================\n"
                 "\n\n\n\n\n\n\n"
                 "     db    db  .d88b.  d888888b  .o88b. d88888b      .d8b.  d8888b. d8888b.     \n"
                 "     88    88 .8P  Y8.   `88'   d8P  Y8 88'         d8' `8b 88  `8D 88  `8D     \n"
                 "     Y8    8P 88    88    88    8P      88ooooo     88ooo88 88oodD' 88oodD'     \n"
                 "     `8b  d8' 88    88    88    8b      88~~~~~     88~~~88 88~~~   88~~~       \n"
                 "      `8bd8'  `8b  d8'   .88.   Y8b  d8 88.         88   88 88      88          \n"
                 "        YP     `Y88P'  Y888888P  `Y88P' Y88888P     YP   YP 88      88          \n"
                 "\n\n\n"
                 "--------------------------- Version and release info ---------------------------\n"
                 "\n"
                 "       << We call it beta version because it is betta than nothing.(c) >>       \n"
                 "\n\n"
                 "Current version is 0.04 build 37 beta [0.04.0032:22-11-2013].\n"
                 "\n"
                 "This application and it's source code are considered to comply with GNUv2 GPL.  \n"
                 "That means you can do whatever you want with it. GPL gives you permission to ma-\n"
                 "ke and redistribute copies of the program if you chooses to do so. You also have\n"
                 "the right not to redistribute the program, if that is what you choose.\n"
                 "\n\n"
                 "------------------------- Command Line Interface usage -------------------------\n"
                 "\n"
                 "##  General command & switches\n"
                 "--------\n"
                 "[0]     @app -u [--load] <samples_path>\n"
                 "        Record samples within the microphone, encode and save them to binary fi-\n"
                 "        le accessed by <samples_path> path.\n"
                 "--------\n"
                 "[1]     @app -p [--proc] <samples_path> <mfcc_path>\n"
                 "        Gather samples from binary file with <samples_path> path, process them,\n"
                 "        and save the encoded resulting mel-cepstral coefficients vector to bina-\n"
                 "        ry file accessed by <mfcc_path> path.\n"
                 "--------\n"
                 "[2]     @app -l [--lrn] <mfcc_path> <user_name>\n"
                 "        Gather MFCC vector from binary file with <mfcc_path> path and adjust the\n"
                 "        MFCC vector of the user with name <user_name> given read vector. Databa-\n"
                 "        se would be updated and resaved.\n"
                 "--------\n"
                 "[3]     @app d [--idtf] <mfcc_path>\n"
                 "        Gather MFCC vector from binary file with <mfcc_path> path and calculate \n"
                 "        the most probable match in the database given users' MFCC vectors.\n"
                 "--------\n"
                 "[4]     @app -? [-h, --man]\n"
                 "        View this application manual.\n"
                 "\n"
                 "##  System configuration management\n"
                 "--------\n"
                 "[5]     @app -c [--set, --conf] <param_name>\n"
                 "        Show the current value of the system parameter with name <param_name>.\n"
                 "--------\n"
                 "[6]     @app -c [--set, --conf] <param_name> <param_value>\n"
                 "        Reset the current value of the system parameter with name <param_name>\n"
                 "        with the value specified by <param_value>.\n"
                 "\n"
                 "##  System configuration parameters (not working in beta)\n"
                 "--------\n"
                 "[0]     -SF [--smplfreq]\n"
                 "        The sampling frequency of the recorded audio file. This parameter af-\n"
                 "        fects the calculation of the MFCC coefficients given the array of samp-\n"
                 "        les.\n"
                 "--------\n"
                 "[1]     -FFTS [--fftsize]\n"
                 "        The size of the array which is being processed by the Fast Fourier Engi-\n"
                 "        ne, must be the power of two. Application does internal verification of\n"
                 "        the value passed as <param_value>.\n"
                 "--------\n"
                 "[2]     -D [--dimns]\n"
                 "        The amount of dimensions in the MFCC vector associated with each user \n"
                 "        and used when calculating distance and match probability.\n"
                 "--------\n"
                 "[3]     -DDBP [--dfdb]\n"
                 "        Path to the database - binary file which is used to store the informati-\n"
                 "        on about users, including their MFCC vectors, between the calls of the\n"
                 "        application.\n"
                 "\n"
                 "##  Database management\n"
                 "--------\n"
                 "[7]     @app -b [--db] -s [--show] (--all)\n"
                 "        Load and view whole database in plaintext mode.\n"
                 "--------\n"
                 "[8]     @app -b [--db] --add (--rem, --show [-s], --all) <user_name>\n"
                 "        Add, remove or view the info about the account with name <user_name> in\n"
                 "        the database if one exists.\n"
                 "        -all switch forces app to view all MFCC vector submits for given user.\n"
                 "\n\n"
                 "----------------------------- About the developers -----------------------------\n"
                 "\n"
                 "Authors and current copyright holders are students of Moscow State Technical \n"
                 "University named after Bauman N. E.:\n"
                 "--------\n"
                 "[0]     Arseniy Aprelev (aka Aaron West)\n"
                 "        aaron.adam.west@gmail.com\n"
                 "--------\n"
                 "[1]     Catherine Shostak\n"
                 "        queenliestme@mail.ru\n"
                 "\n"
                 "Feel free to contact the developers about any bugs in the software. We do care.\n"
                 "================================================================================\n");
    return;
}

void interface::quit() {
    cout << "[Started quitting sequence]\n";
    if (logic::initialised() && data::loaded())
        data::savedb(configs::dbpath());
    cout << "[Quitting application]\n";
    return;
}











