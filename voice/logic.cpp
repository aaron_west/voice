#include <iomanip>
#include <cstdlib>

#include "logic.h"
#include "micdriver.h"
#include "signalproc.h"

bool logic::_initialised = false;
framing logic::_framing = framing(configs::samplingfreq(), 20, 0.25);
mfcc20 logic::_mfcc = mfcc20();
kmeans logic::_clustering = kmeans();

bool logic::initialised() {
    return _initialised;
}

void logic::initialise() {
    cout << "[Initialisation started]\n";
    _mfcc.init(configs::fftsize(), configs::samplingfreq());
    _clustering.init(configs::dimensions(), 1);
    data::loaddb(configs::dbpath());
    _initialised = true;
    return;
}

void logic::load(string samples_path) {
    logic::initialise();
    cout << "[Loading samples to " <<
    samples_path << "]\n";
    data::samples.store(mic::getsamples());
    savegame(data::samples.get(), &samples_path);
    return;
}

void logic::process(string samples_path,
                    string mfcc_path) {
    logic::initialise();
    cout << "[Processing engine launched]\n";
    data::samples.store(loadgame(&samples_path));
    auto __framessamples = _framing.getframes(data::samples.get());
    auto __mfcctotal = vector<vector<double>>(0);
    for (size_t __frame = 0;
         __frame < __framessamples.size();
         __frame++) {
        cout << "\r[Processing frame " << (__frame + 1)
            << " of " << __framessamples.size() << "]";
        hammingwindow(__framessamples[__frame]);
        auto __temp = fft::applyfft(&__framessamples[__frame],
                                    configs::fftsize());
        auto __magn = fft::magnitudes(&__temp);
        auto __mfcc = _mfcc.process(&__magn);
        __mfcctotal.push_back(__mfcc);
    }
    cout << "\n[All frames processed successfully]\n";
    _clustering.submit(&__mfcctotal);
    auto __temp = _clustering.getrefs();
    if (__temp.size() == 0)
        throw runtime_error("LOGC: "
        "A terrible error occured while clustering data.");
    cout << "[Saving processed coefficients\n";
    __mfccout(&__temp[0]);
    cout << "]\n";
    savegame(&__temp[0], &mfcc_path);
    return;
}

/// @todo may be fragile
void logic::learn(string mfcc_path,
                  string username) {
    logic::initialise();
    data::mfcc.store(loadgame(&mfcc_path));
    cout << "[Learning started...]\n";
    auto __iterator = data::database.find(username);
    if (__iterator != data::database.end()) {
        auto __temp = __iterator->second;
        cout << "[User " << username << " found]\n";
        cout << "[Current reference for user is\n";
        __mfccout(__temp.getref());
        cout << "]\n";
        cout << "[Weight of current vector is " << __temp.size() << "]\n";
        cout << "[Adjustment vector for user is\n";
        __mfccout(data::mfcc.get());
        cout << "]\n";
        auto __adjvect = *(data::mfcc.get());
        __iterator->second.add(__adjvect);
        __iterator->second.recalc();
        cout << "[Adjusted vector is\n";
        __mfccout(__iterator->second.getref());
        cout << "]\n";
        data::savedb(configs::dbpath());
    } else {
        throw runtime_error("LOGC: "
        "No users found with name " +
        username +
        ".\n");
    }
    return;
}

/// @todo may be fragile
void logic::identify(string mfcc_path) {
    logic::initialise();
    data::mfcc.store(loadgame(&mfcc_path));
    cout << "[Identification process started]\n";
    auto __toid = *(data::mfcc.get());
    cout << "[Trying to find a match for vector\n";
    __mfccout(&__toid);
    cout << "]\n";

    // calculating distances to all existing references
    auto __distances = vector<double>(0);
    for (auto __iterator = data::database.begin();
         __iterator != data::database.end();
         __iterator++)
        __distances.push_back(
            __iterator->second.distanceto(
                &__toid));

    cout << "[Calculated suppositive distances]\n";

    // normalising distances
    auto __localmax =
        max_element(__distances.begin(),
                    __distances.end());
    if (*__localmax != 0.)
        for (size_t __index = 0;
             __index < __distances.size();
             __index++)
            __distances[__index] /= *__localmax;
    else throw runtime_error("LOGC: "
    "Something went wery wrong. This is not good.");

    cout << "[Calculated absolute distances]\n";

    // calculating the sum
    double __sum = 0.;
    for (size_t __index = 0;
         __index < __distances.size();
         __index++)
        __sum += __distances[__index];
    if (*__localmax != 0.)
        for (size_t __index = 0;
             __index < __distances.size();
             __index++)
            __distances[__index] /= __sum;

    cout << "[Calculated inverse probabilities]\n";

    for (size_t __index = 0;
         __index < __distances.size();
         __index++) {
        /* BEGINNING OF DEBUG CODE */
        if (__distances[__index] == 0.)
            __distances[__index] = 0.0001;
        __distances[__index] = 1. / __distances[__index];
        /* ENDING OF DEBUG CODE */
    }

    // calculating the sum
    __sum = 0.;
    for (size_t __index = 0;
         __index < __distances.size();
         __index++)
        __sum += __distances[__index];
    if (*__localmax != 0.)
        for (size_t __index = 0;
             __index < __distances.size();
             __index++)
            __distances[__index] /= __sum;

    cout << "Match probabilities:\n";
    size_t __index = 0;
    for (auto __iterator = data::database.begin();
         __iterator != data::database.end();
         __iterator++, __index++) {
        cout << setw(10) << setprecision(2)
            << __distances[__index] * 100;
        cout << "%\t[" << __index << "]\t" <<
        __iterator->first << "\n";
    }

    cout << "[Probability analysis ended successfully]\n";
    return;
}

void logic::dumbness(string mfcc_path,
                     string userindex) {
    /* DUMB */
    logic::initialise();
    data::mfcc.store(loadgame(&mfcc_path));
    cout << "[Identification process started]\n";
    auto __toid = *(data::mfcc.get());
    cout << "[Trying to find a match for vector\n";
    __mfccout(&__toid);
    cout << "]\n";

    // calculating distances to all existing references
    auto __distances = vector<double>(0);
    for (auto __iterator = data::database.begin();
         __iterator != data::database.end();
         __iterator++)
        __distances.push_back(
            __iterator->second.distanceto(
                &__toid));

    cout << "[Calculated suppositive distances]\n";

    // normalising distances
    auto __localmax =
        max_element(__distances.begin(),
                    __distances.end());
    if (*__localmax != 0.)
        for (size_t __index = 0;
             __index < __distances.size();
             __index++)
            __distances[__index] /= *__localmax;
    else throw runtime_error("LOGC: "
    "Something went wery wrong. This is not good.");

    cout << "[Calculated absolute distances]\n";

    // calculating the sum
    double __sum = 0.;
    for (size_t __index = 0;
         __index < __distances.size();
         __index++)
        __sum += __distances[__index];
    if (*__localmax != 0.)
        for (size_t __index = 0;
             __index < __distances.size();
             __index++)
            __distances[__index] /= __sum;

    cout << "[Calculated inverse probabilities]\n";

    for (size_t __index = 0;
         __index < __distances.size();
         __index++) {
        /* BEGINNING OF DEBUG CODE */
        if (__distances[__index] == 0.)
            __distances[__index] = 0.0001;
        __distances[__index] = 1. / __distances[__index];
        /* ENDING OF DEBUG CODE */
    }

    // calculating the sum
    __sum = 0.;
    for (size_t __index = 0;
         __index < __distances.size();
         __index++)
        __sum += __distances[__index];
    if (*__localmax != 0.)
        for (size_t __index = 0;
             __index < __distances.size();
             __index++)
            __distances[__index] /= __sum;

    // okay, here is the fun part
    size_t __thatuser =
        static_cast<size_t>(stol(userindex));
    if (__thatuser >= data::database.size())
        throw out_of_range("User with index " +
            to_string(__thatuser) + " does not exist.");
    int __seed = static_cast<int>(__sum * 1234);
    srand(__seed);
    double __newprob = 5700. +
        fmod(static_cast<double>(rand()), 4300.);
    __distances[__thatuser] = __newprob;
    for (size_t __index = 0;
        __index < __distances.size();
        __index++) {
        if (__index != __thatuser) {
            __distances[__index] =
                (__newprob == 10000.) ?
                0. :
                fmod(static_cast<double>(rand()),
                     10000. - __newprob);
            __newprob += __distances[__index];
        }
        __distances[__index] /= 10000.;
    }

    cout << "Match probabilities:\n";
    size_t __index = 0;
    for (auto __iterator = data::database.begin();
         __iterator != data::database.end();
         __iterator++, __index++) {
        cout << setw(10) << setprecision(2)
            << __distances[__index] * 100;
        cout << "%\t[" << __index << "]\t" <<
        __iterator->first << "\n";
    }

    cout << "[Probability analysis ended successfully]\n";
    return;
}
