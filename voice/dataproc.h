#ifndef __voice__dataproc__
#define __voice__dataproc__

#include <vector>

using namespace std;
enum avrgmeths : unsigned int {
    arithmeticm,
    rms
};

double average(avrgmeths method,
               vector<double> *data);

vector<double> fillblock(vector<double> *data,
                         unsigned long block);

class framing {
private:
    double _lengthms;
    double _overlapperc;
    unsigned int _length;
    unsigned int _overlap;
    unsigned int _samplfreq;
    void _recalc();
public:
    framing(unsigned int frequency);
    framing(unsigned int frequency,
            double lengthms,
            double overlapperc);
    inline void setlengthms(double length);
    inline void setoverlapperc(double overlapperc);
    vector<double> getframe(size_t frame,
                            vector<double> *data);
    vector<vector<double>> getframes(vector<double> *data);
};



#endif /* defined(__voice__dataproc__) */
