#ifndef __voice__micdriver__
#define __voice__micdriver__

#include <vector>
using namespace std;

class mic {
public:
    static vector<double> getsamples();
    static vector<double> getsamples(size_t buffersize,
                                     size_t samplefreq,
                                     uint8_t channels);
};

#endif
