#ifndef __voice__configs__
#define __voice__configs__

#define SAMPLING_FREQUENCY  44100
#define FFT_SIZE            1024
#define DIMENSIONS          13
#define DEFAULT_DBPATH      "/home/aaronwest/Documents/code::blocks/voice test/voice/bin/Debug/db.bin"

#include <string>

using namespace std;

class configs {
private:
    static uint32_t _samplingfreq;
    static uint32_t _fftsize;
    static uint32_t _dimensions;
    static string _dbpath;
public:
    static uint32_t samplingfreq();
    static void samplingfreq(uint32_t freq);
    static uint32_t fftsize();
    static void fftsize(uint32_t fftsize);
    static uint32_t dimensions();
    static void dimensions(uint32_t dimensions);
    static string dbpath();
    static void dbpath(string dbpath);
};

#endif
