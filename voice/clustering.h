#ifndef __voice__clustering__
#define __voice__clustering__

#include <vector>

using namespace std;

class cluster {
protected:
    vector<vector<double>> _data;
    vector<double> _reference;
public:
    cluster(unsigned int _dimensions);
    size_t size();
    void add(vector<double> pointData);
    vector<vector<double>>* getpoints();
    vector<double>* getref();
    inline void setref(vector<double> *_reference);
    void recalc();
    void remove(size_t point);
    double distanceto(vector<double> *point);
};

class kmeans {
private:
    bool _initialized;
    unsigned int _dimensions;
    unsigned int _clustersnum;
    std::vector<cluster> _clusters;
    void randomise(vector<vector<double>> *data);
    size_t choose(vector<double> *point);
    void initialpart(vector<vector<double>> *data);
    void recalc();
    void move();
    bool allright();
public:
    kmeans();
    void init(unsigned int dimensions, unsigned int clusters);
    void submit(vector<vector<double>> *data);
    vector<vector<double>> getrefs();
};

#endif /* defined(__voice__clustering__) */
