#include <cmath>
#include <string>
#include <limits>
#include <iomanip>
#include <iostream>
#include <stdexcept>

#include "dataproc.h"

using namespace std;



//     ###    ##     ## ######## ########   ######
//    ## ##   ##     ## ##       ##     ## ##    ##
//   ##   ##  ##     ## ##       ##     ## ##
//  ##     ## ##     ## ######   ########  ##   ####
//  #########  ##   ##  ##       ##   ##   ##    ##
//  ##     ##   ## ##   ##       ##    ##  ##    ##
//  ##     ##    ###    ######## ##     ##  ######

double average(avrgmeths method, vector<double> *data) {
    if (data == nullptr)
        throw invalid_argument("DPRC: "
        "Pointer provided is invalid.");
    if ((*data).empty() ||
        (*data).size() == 0)
        throw logic_error("DPRC: "
        "The data container provided is empty.");
    double __average = 0.;
    switch (method) {
        case avrgmeths::rms:
        {
            double __minimum = 0., __maximum = 0., __temp = 0.;
            for (size_t __counter = 0;
                 __counter < (*data).size();
                 __counter++) {
                __temp = __average;
                __average += ((*data)[__counter] *
                              (*data)[__counter] /
                              (*data).size());
                if (__average < __temp)
                    throw overflow_error("DPRC: "
                    "Double type overflowed.");
                else if ((*data)[__counter] > __maximum)
                    __maximum = (*data)[__counter];
                else if ((*data)[__counter] < __minimum)
                    __minimum = (*data)[__counter];
            }
            __average = sqrt(__average);
            if (__maximum + __minimum < 0)
                __average = 0. - __average;
        }
            break;
        case avrgmeths::arithmeticm:
        {
            for (size_t __counter = 0;
                 __counter < (*data).size();
                 __counter++)
                __average += (*data)[__counter];
            __average /= (*data).size();
        }
            break;
        default:
            throw out_of_range("DPRC: "
            "Invalid averaging method specified.");
    }
    return __average;
}



//  ########  #######  ########  ##     ##    ###    ########
//  ##       ##     ## ##     ## ###   ###   ## ##      ##
//  ##       ##     ## ##     ## #### ####  ##   ##     ##
//  ######   ##     ## ########  ## ### ## ##     ##    ##
//  ##       ##     ## ##   ##   ##     ## #########    ##
//  ##       ##     ## ##    ##  ##     ## ##     ##    ##
//  ##        #######  ##     ## ##     ## ##     ##    ##

vector<double> fillblock(vector<double> *data,
                         unsigned long block) {
    if (data == nullptr)
        throw invalid_argument("DPRC: "
        "Pointer provided is invalid.");
    if ((*data).empty() ||
        (*data).size() == 0)
        throw logic_error("DPRC: "
        "The data container provided is empty.");
    if (block == 0 ||
        block == NAN)
        throw invalid_argument("DPRC: "
        "The block size parameter provided is invalid.");
    auto clipped = vector<double>(0);
    for (size_t __counter = 0;
         (__counter < block) && (__counter < (*data).size());
         __counter++)
        clipped.push_back((*data)[__counter]);
    for (size_t __counter = clipped.size();
         __counter < block;
         __counter++)
        clipped.push_back(0.);
    return clipped;
}



//  ######## ########     ###    ##     ## ##    ##  ######
//  ##       ##     ##   ## ##   ###   ### ###   ## ##    ##
//  ##       ##     ##  ##   ##  #### #### ####  ## ##
//  ######   ########  ##     ## ## ### ## ## ## ## ##   ####
//  ##       ##   ##   ######### ##     ## ##  #### ##    ##
//  ##       ##    ##  ##     ## ##     ## ##   ### ##    ##
//  ##       ##     ## ##     ## ##     ## ##    ##  ######

void framing::_recalc() {
    _length = _lengthms * _samplfreq / 1000.;
    _overlap = _length * _overlapperc;
    return;
}

framing::framing(unsigned int frequency) {
    if (frequency == NAN)
        throw invalid_argument("DPRC: "
        "Sampling frequency parameter is invalid.");
    _lengthms = 20;
    _overlapperc = 0.5;
    _samplfreq = frequency;
    _recalc();
    return;
}

framing::framing(unsigned int frequency,
                 double lengthms,
                 double overlapperc) {
    if (frequency == NAN)
        throw invalid_argument("DPRC: "
        "Sampling frequency parameter is invalid.");
    if (lengthms == NAN)
        throw invalid_argument("DPRC: "
        "Frame length parameter is invalid.");
    if (overlapperc == NAN)
        throw invalid_argument("DPRC: "
        "Overlap percentage parameter is invalid.");
    _lengthms = lengthms;
    _overlapperc = overlapperc;
    _samplfreq = frequency;
    _recalc();
}

void framing::setlengthms(double length) {
    if (length == NAN)
        throw invalid_argument("DPRC: "
        "Frame length parameter is invalid.");
    _lengthms = length;
    _recalc();
}

void framing::setoverlapperc(double percentage) {
    if ((percentage == NAN) ||
        (percentage > 1.))
        throw invalid_argument("DPRC: "
        "Overlap percentage parameter is invalid.");
    _overlapperc = percentage;
    _recalc();
}

vector<double> framing::getframe(size_t frame,
                                 vector<double> *data) {
    if (data == nullptr)
        throw invalid_argument("DPRC: "
        "Pointer provided is invalid.");
    if ((*data).empty() ||
        ((*data).size() == 0))
        throw logic_error("DPRC: "
        "The data container provided is empty.");
    unsigned long __minsize = frame *
        (_length - _overlap) + _overlap;
    if ((*data).size() < __minsize)
        throw logic_error("DPRC: "
        "Not enough data provided.");
    unsigned long __boundary =
        frame * (_length - _overlap);
    auto __data = vector<double>(_length);
    for (size_t __counter = 0;
         __counter < _length;
         __counter++)
        __data[__counter] =
            (*data)[__boundary + __counter];
    return __data;
}

vector<vector<double>> framing::getframes(vector<double> *data) {
    if (data == nullptr)
        throw invalid_argument(
        "Pointer provided is invalid.");
    if (data->empty() ||
        (data->size() == 0))
        throw logic_error(
        "Empty data container provided.");
    if ((*data).size() < _length)
        throw logic_error(
        "Not enough data provided.");
    if (_length <= _overlap)
        throw logic_error(
        "Frame length is equal or less than frame overlap.");
    cout << "[Frames splitting processor launched: "
    "frame length: " << _lengthms << " ms, overlap percentage: "
    << setfill('0') << setprecision(2) <<
    setw(5) << 100 * _overlapperc << "%]\n";
    auto __frames =
        static_cast<size_t>((*data).size()) - _overlap;
    __frames /= _length - _overlap;
    auto __framesvector = vector<vector<double>>(0);
    for (size_t __frame = 0;
         __frame < __frames;
         __frame++) {
        auto __data = getframe(__frame, data);
        __framesvector.push_back(__data);
    }
    return __framesvector;
}
