#ifndef __voice__storage__
#define __voice__storage__

#include <vector>
#include <stdexcept>

using namespace std;

template <typename __type>
class storage {
private:
    bool _stored;
    vector<__type> _data;
public:
    inline bool stored() {
        return _stored;
    }
    vector<__type>* get();
    storage() {
        _stored = false;
    }
    void store(vector<__type> &data);
    void store(vector<__type> *data);
    void store(vector<__type> data);
    size_t size();
    size_t memory();
};

template <typename __type>
vector<__type>* storage<__type>::get() {
    if (_stored)
        return &_data;
    else
        throw runtime_error("STOR: "
        "Trying to access non-initialised data.");
}

template <typename __type>
void storage<__type>::store(vector<__type> &data) {
    cout << "[Storing " << data.size() <<
    " values in the internal memory]\n";
    try {
        size_t __allocated = 0;
        if (_stored) {
            __allocated = memory();
            _data.clear();
            _stored = false;
        }
        _data = vector<__type>(data);
        __allocated =
            sizeof(__type) * _data.size();
        _stored = true;
    } catch (const exception &exc) {
        _stored = false;
        throw runtime_error("STOR: " +
        (string)exc.what());
    }
}

template <typename __type>
void storage<__type>::store(vector<__type> *data) {
    cout << "[Storing " << data->size() <<
    " values in the internal memory]\n";
    try {
        if (_stored) {
            _data.clear();
            _stored = false;
        }
        _data = vector<__type>(*data);
        _stored = true;
    } catch (const exception &exc) {
        _stored = false;
        throw runtime_error("STOR: " +
        (string)exc.what());
    }
}

template <typename __type>
void storage<__type>::store(vector<__type> data) {
    cout << "[Storing " << data.size() <<
    " values in the internal memory]\n";
    try {
        if (_stored) {
            _data.clear();
            _stored = false;
        }
        _data = vector<__type>(data);
        _stored = true;
    } catch (const exception &exc) {
        _stored = false;
        throw runtime_error("STOR: " +
        (string)exc.what());
    }
}

template <typename __type>
size_t storage<__type>::size() {
    if (_stored)
        return _data.size();
    else
        return 0;
}

template <typename __type>
size_t storage<__type>::memory() {
    if (_stored)
        return (_data.size() * sizeof(__type));
    else
        return 0;
}

#endif
