#include <cmath>
#include <cstdlib>
#include <string>
#include <iostream>
#include <stdexcept>
#include <limits>

#include "clustering.h"
#include "dataproc.h"

using namespace std;



//   ######  ##       ##     ##  ######  ######## ########
//  ##    ## ##       ##     ## ##    ##    ##    ##     ##
//  ##       ##       ##     ## ##          ##    ##     ##
//  ##       ##       ##     ##  ######     ##    ########
//  ##       ##       ##     ##       ##    ##    ##   ##
//  ##    ## ##       ##     ## ##    ##    ##    ##    ##
//   ######  ########  #######   ######     ##    ##     ##

void cluster::recalc() {
    auto __data = vector<double>(0);
    double __average = 0.;
    for (size_t __dimension = 0;
         __dimension < _reference.size();
         __dimension++) {
        __data.clear();
        for (size_t __index = 0;
             __index < _data.size();
             __index++)
            __data.push_back(_data[__index][__dimension]);
        __average = average(avrgmeths::arithmeticm,
                            &__data);
        _reference[__dimension] = __average;
    }
    return;
}

cluster::cluster(unsigned int _dimensions) {
    if (_dimensions == NAN)
        throw invalid_argument("CLST: "
        "Dimensions amount parameter is invalid.");
    _data = vector<vector<double>>(0);
    _reference = vector<double>(_dimensions);
}

size_t cluster::size() {
    return _data.size();
}

void cluster::add(vector<double> pointData) {
    if (pointData.empty() ||
        (pointData.size() == 0))
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if (pointData.size() != _reference.size())
        throw logic_error("CLST: "
        "Dimensions amount mismatch.");
    _data.push_back(pointData);
}

vector<vector<double>>* cluster::getpoints() {
    return &_data;
}

vector<double>* cluster::getref() {
    return &_reference;
}

void cluster::setref(vector<double> *reference) {
    if (reference == nullptr)
        throw invalid_argument("CLST: "
        "Pointer provided is invalid.");
    if (reference->empty() ||
        (reference->size() == 0))
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if (reference->size() != _reference.size())
        throw logic_error("CLST: "
        "Dimensions amount mismatch.");
    _reference = vector<double>(*reference);
}

void cluster::remove(size_t point) {
    if (point >= _data.size())
        throw out_of_range("CLST: "
        "Element index out of bounds.");
    _data.erase(_data.begin() + point);
}

double cluster::distanceto(vector<double> *point) {
    if (point == nullptr)
        throw invalid_argument("CLST: "
        "Pointer provided is invalid.");
    if (((*point).size() == 0) ||
        (point->empty()))
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if ((*point).size() != _reference.size())
        throw logic_error("CLST: "
        "Dimensions amount mismatch.");
    double __distance = 0.;
    auto __distances = vector<double>(_reference.size());
    for (size_t __dimension = 0;
         __dimension < __distances.size();
         __dimension++) {
        double __temp = (*point)[__dimension] -
            _reference[__dimension];
        __temp = abs(__temp);
        __distances[__dimension] = __temp;
        __distance += pow(__distances[__dimension], 2.);
    }
    __distance = sqrt(__distance);
    return __distance;
}



//  ##    ##         ##     ## ##    ##  ######
//  ##   ##          ###   ### ###   ## ##    ##
//  ##  ##           #### #### ####  ## ##
//  #####    ####### ## ### ## ## ## ##  ######
//  ##  ##           ##     ## ##  ####       ##
//  ##   ##          ##     ## ##   ### ##    ##
//  ##    ##         ##     ## ##    ##  ######

kmeans::kmeans() {
    _dimensions = 0;
    _clustersnum = 0;
    _clusters.clear();
    _initialized = false;
}

void kmeans::init(unsigned int dimensions,
                  unsigned int clusters) {
    if (dimensions == NAN)
        throw invalid_argument("CLST: "
        "Dimensions amount parameter is invalid.");
    if (clusters == NAN)
        throw invalid_argument("CLST: "
        "Clusters amount parameter is invalid.");
    cout << "[Initialising kmeans clustering processor]\n";
    _dimensions = dimensions;
    _clustersnum = clusters;
    if (_initialized) {
        _clusters.clear();
        _initialized = false;
    }
    for (size_t __counter = 0;
         __counter < _clustersnum;
         __counter++) {
        auto __cluster = cluster(dimensions);
        _clusters.push_back(__cluster);
    }
    _initialized = true;
}

void kmeans::randomise(vector<vector<double>> *data) {
    if (data == nullptr)
        throw invalid_argument("CLST: "
        "Pointer provided is invalid.");
    if (((*data).size() == 0) ||
        (data->empty()))
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if (!_initialized) {
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    }
    for (size_t __cluster = 0;
         __cluster < _clusters.size();
         __cluster++) {
        int index = rand() % (*data).size();
        _clusters[__cluster].setref(&(*data)[index]);
    }
}

size_t kmeans::choose(vector<double> *point) {
    if (point == nullptr)
        throw invalid_argument("CLST: "
        "Pointer provided is invalid.");
    if (((*point).size() == 0) ||
        (point->empty()))
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if ((*point).size() != _dimensions)
        throw logic_error("CLST: "
        "Dimensions amount mismatch.");
    size_t __cluster = 0;
    double __distance = 0.,
        __mindist = numeric_limits<double>::max();
    for (size_t __counter = 0;
         __counter < _clusters.size();
         __counter++) {
        __distance =
            _clusters[__counter].distanceto(point);
        if (abs(__distance) < abs(__mindist)) {
            __mindist = __distance;
            __cluster = __counter;
        }
    }
    return __cluster;
}

void kmeans::initialpart(vector<vector<double>> *data) {
    if (data == nullptr)
        throw invalid_argument("CLST: "
        "Pointer provided is invalid.");
    if (data->empty() ||
        (data->size() == 0))
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if (!_initialized)
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    cout << "[Performing random initial partition]\n";
    size_t __cluster = 0;
    for (size_t __index = 0;
         __index < data->size();
         __index++) {
        __cluster = choose(&((*data)[__index]));
        _clusters[__cluster].add((*data)[__index]);
    }
    return;
}

void kmeans::recalc() {
    if (!_initialized)
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    for (size_t __cluster = 0;
         __cluster < _clusters.size();
         __cluster++)
        _clusters[__cluster].recalc();
    return;
}

void kmeans::move() {
    if (!_initialized)
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    for (size_t __cluster = 0;
         __cluster < _clusters.size();
         __cluster++) {
        auto __tempvector = *(_clusters[__cluster].getpoints());
        for (size_t __point = 0;
             __point < __tempvector.size();
             __point++)
            if (choose(&__tempvector[__point]) != __cluster) {
                auto __data = __tempvector[__point];
                size_t __gaining = choose(&__data);
                _clusters[__cluster].remove(__point);
                _clusters[__gaining].add(__data);
                return;
            }
        __tempvector.clear();
    }
    return;
}

bool kmeans::allright() {
    if (!_initialized)
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    for (size_t __cluster = 0;
         __cluster < _clusters.size();
         __cluster++) {
        auto __tempvector = *(_clusters[__cluster].getpoints());
        for (size_t __point = 0;
             __point < __tempvector.size();
             __point++)
            if (choose(&__tempvector[__point]) != __cluster)
                return false;
    }
    cout << "[Clustering processor finished processing data]\n";
    return true;
}

void kmeans::submit(vector<vector<double>> *data) {
    if (data == nullptr)
        throw invalid_argument("CLST: "
        "Pointer provided is invalid.");
    if ((*data).empty() ||
        (*data).size() == 0)
        throw logic_error("CLST: "
        "The data container provided is empty.");
    if (!_initialized)
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    cout << "[Data submitted to kmeans processor; "
    "size: " << data->size() << "]\n";
    randomise(data);
    initialpart(data);
    while (!allright()) {
        recalc();
        move();
    }
    return;
}

vector<vector<double>> kmeans::getrefs() {
    if (!_initialized)
        throw logic_error("CLST: "
        "KMProcessor instance is not initialised.");
    cout << "[Gathering reference points]\n";
    auto __references = vector<vector<double>>(0);
    for (size_t __cluster = 0;
         __cluster < _clusters.size();
         __cluster++)
        __references.push_back(*_clusters[__cluster].getref());
    return __references;
}
