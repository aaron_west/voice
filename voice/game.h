#ifndef __voice__game__
#define __voice__game__

#include <vector>
#include <map>
#include <string>
#include <cmath>
#include <fstream>
#include <sstream>

#include "clustering.h"

using namespace std;

bool valid(string *path);

bool exists(string *path);

vector<uint8_t> __serialise(double value);

double __deserialize(vector<uint8_t> binary);

vector<uint8_t> __tobytearray(vector<double> doublearray);

vector<double> __todoublearray(vector<uint8_t> bytearray);

class __base64 {
public:
    static vector<uint8_t> decode (string in_data);
    static string encode (vector<uint8_t> in_data);
};

void savegame(vector<double> *game, string *to);

vector<double>* loadgame(string *from);

void __savedb(map<string, cluster> *db, string *to);

map<string, cluster>* __loaddb(string *from);

#endif
