#include <string>
#include <iostream>
#include <stdexcept>
#include <pulse/simple.h>
#include <pulse/error.h>

#include "micdriver.h"


using namespace std;

vector<double> mic::getsamples() {
    cout << "[Accessed MICD interface]\n";
    const size_t __buffersize = 44100 * 3;
    auto __samples = vector<double>(__buffersize);
    try {
        static const pa_sample_spec __format = {
            PA_SAMPLE_U8,
            44100,
            1};
        pa_simple *__stream = nullptr;
        int __error = 0;
        __stream = pa_simple_new(nullptr,
                                 "voice app",
                                 PA_STREAM_RECORD,
                                 nullptr,
                                 "MICD stream",
                                 &__format,
                                 nullptr,
                                 nullptr,
                                 &__error);
        if (!__stream) {
            pa_simple_free(__stream);
            throw runtime_error(pa_strerror(__error));
        }
        uint8_t __buffer[__buffersize];
        cout << "[Starting recording process...]\n";
        for(size_t __index = 0;
            __index < __buffersize;
            __index++) {
            if (pa_simple_read(__stream,
                                &__buffer[__index],
                                sizeof(uint8_t),
                                &__error) < 0)
                throw runtime_error(pa_strerror(__error));
                cout << "\r[Reading sample " <<
                    __index << " out of " <<
                        __buffersize << "]";
            }
        cout << "\n[All samples recorded]\n";
        size_t __index = 0;
        for (auto __iterator = __samples.begin();
            __iterator != __samples.end();
            __iterator++, __index++)
            *__iterator = static_cast<double>(__buffer[__index]);
    } catch (exception &exc) {
        throw runtime_error("MICD: " +
        (string)exc.what() + "\n");
    }
    return __samples;
}

vector<double> mic::getsamples(size_t buffersize,
                               size_t samplefreq,
                               uint8_t channels) {
    cout << "[Accessed custom MICD interface]\n";
    cout << "[Buffer size: " << buffersize
        << ", sampling frequency: " << samplefreq
        << ", amount of channels: " << channels
        << "]\n";
    const size_t __buffersize = buffersize;
    auto __samples = vector<double>(__buffersize);
    try {
        static const pa_sample_spec __format = {
            PA_SAMPLE_U8,
            samplefreq,
            channels};
        pa_simple *__stream = nullptr;
        int __error = 0;
        __stream = pa_simple_new(nullptr,
                                 "voice app",
                                 PA_STREAM_RECORD,
                                 nullptr,
                                 "MICD stream",
                                 &__format,
                                 nullptr,
                                 nullptr,
                                 &__error);
        if (!__stream) {
            pa_simple_free(__stream);
            throw runtime_error(pa_strerror(__error));
        }
        uint8_t *__buffer = new uint8_t[__buffersize];
        cout << "[Starting recording process...]\n";
        for(size_t __index = 0;
            __index < __buffersize;
            __index++) {
            if (pa_simple_read(__stream,
                                &__buffer[__index],
                                sizeof(uint8_t),
                                &__error) < 0)
                throw runtime_error(pa_strerror(__error));
                cout << "\r[Reading sample " <<
                    __index << " out of " <<
                    __buffersize << "]";
            }
        cout << "\n[All samples recorded]\n";
        size_t __index = 0;
        for (auto __iterator = __samples.begin();
            __iterator != __samples.end();
            __iterator++, __index++)
            *__iterator = static_cast<double>(__buffer[__index]);
    } catch (exception &exc) {
        throw runtime_error("MICD: " +
        (string)exc.what() + "\n");
    }
    return __samples;
}
