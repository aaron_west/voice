#include <iostream>
#include <stdexcept>

#include "game.h"
#include "configs.h"

using namespace std;

//     ###    ##     ## ##     ## ##       ########  ##    ##
//    ## ##   ##     ##  ##   ##  ##       ##     ##  ##  ##
//   ##   ##  ##     ##   ## ##   ##       ##     ##   ####
//  ##     ## ##     ##    ###    ##       ########     ##
//  ######### ##     ##   ## ##   ##       ##   ##      ##
//  ##     ## ##     ##  ##   ##  ##       ##    ##     ##
//  ##     ##  #######  ##     ## ######## ##     ##    ##

bool valid(string *path) {
    try {
        filebuf __buffer;
        __buffer.open(*path, ios::in);
        if (__buffer.is_open())
            __buffer.close();
    } catch (exception &exc) {
        return false;
    }
    return true;
}

bool exists(string *path) {
    bool __exists = false;
    ifstream f((*path).c_str());
    if (f.good())
        __exists = true;
    f.close();
    return __exists;
}



//  ########  ########  ##          ######## ######## ##
//  ##     ## ##     ## ##          ##    ## ##       ##    ##
//  ##     ## ##     ## ##              ##   ##       ##    ##
//  ##     ## ########  ##             ##    #######  ##    ##
//  ##     ## ##     ## ##            ##           ## #########
//  ##     ## ##     ## ##            ##     ##    ##       ##
//  ########  ########  ########      ##      ######        ##

vector<uint8_t> __serialise(double value) {
    auto __bytes = vector<uint8_t>(12);
    fill(__bytes.begin(), __bytes.end(), 0x00);
    if (value == 0.)
        return __bytes;
    double __fraction = 0.;
    int __exponent = 0, __sign = value > 0 ? 0 : 1;
    uint64_t __significand = (1ULL << 51);
    if (value < 0)
        value = 0. - value;
    __fraction = frexp(value, &__exponent);
    __significand *= __fraction;
    for (size_t __byte = 0;
         __byte < 8;
         __byte++) {
        uint8_t __current = (__significand >> (__byte * 8)) & 0xFF;
        __bytes[11 - __byte] = __current;
    }
    auto __exp16= static_cast<int16_t>(__exponent);
    for (size_t __byte = 0;
         __byte < 2;
         __byte++) {
        uint8_t __current = __exp16 >> __byte * 8 & 0xFF;
        __bytes[3 - __byte] = __current;
    }
    __bytes[0] = static_cast<uint8_t>(__sign);
    return __bytes;
}

double __deserialize(vector<uint8_t> binary) {
    double __value = 0.;
    int16_t __exp16 = 0;
    int __exponent = 0, __sign = 0;
    uint64_t __significand = 0;
    bool __allnull = true;
    for (size_t __byte = 0;
         __byte < 12;
         __byte++)
        if (binary[__byte] != 0)
            __allnull = false;
    if (__allnull)
        return __value;
    for (size_t __byte = 0;
         __byte < 8;
         __byte++) {
        uint64_t __current = binary[11 - __byte];
        __significand |= (__current << (__byte * 8));
    }
    uint64_t __temp = 1ULL << 51;
    double __fraction = __significand / (double)__temp;
    for (size_t __byte = 0;
         __byte < 2;
         __byte++) {
        uint8_t __current = binary[3 - __byte];
        __exp16 |= __current << __byte * 8;
    }
    __exponent = static_cast<int>(__exp16);
    __value = ldexp(__fraction, __exponent);
    __sign = static_cast<int>(binary[0]);
    if (__sign != 0)
        __value = 0. - __value;
    return __value;
}

vector<uint8_t> __tobytearray(vector<double> doublearray) {
    auto __bytearray = vector<uint8_t>(12 * doublearray.size());
    for (size_t __double = 0;
         __double < doublearray.size();
         __double++) {
        auto __temp = __serialise(doublearray[__double]);
        copy(__temp.begin(),
             __temp.end(),
             __bytearray.begin() + 12 * __double);
    }
    return __bytearray;
}

vector<double> __todoublearray(vector<uint8_t> bytearray) {
    vector<double> __doublearray;
    vector<uint8_t> __temp;
    for (size_t __double = 0;
         __double < bytearray.size();
         __double += 12) {
        __temp.clear();
        __temp.resize(12);
        copy(bytearray.begin() + __double,
             bytearray.begin() + __double + 12,
             __temp.begin());
        __doublearray.push_back(__deserialize(__temp));
    }
    return __doublearray;
}



//  ########     ###     ######  ########     #######  ##
//  ##     ##   ## ##   ##    ## ##          ##     ## ##    ##
//  ##     ##  ##   ##  ##       ##          ##        ##    ##
//  ########  ##     ##  ######  ######      ########  ##    ##
//  ##     ## #########       ## ##          ##     ## #########
//  ##     ## ##     ## ##    ## ##          ##     ##       ##
//  ########  ##     ##  ######  ########     #######        ##

static const string __alphabet =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

static inline bool __valid(unsigned char c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
}

string __base64::encode (vector<uint8_t> what) {
    string __encoded = "";
    size_t __i = 0;
    size_t __j = 0;
    uint8_t *__pointer = &what[0];
    size_t __length = what.size();
    uint8_t __char3[3];
    uint8_t __char4[4];
    while (__length--) {
        __char3[__i++] = *(__pointer++);
        if (__i == 3) {
            __char4[0] = (__char3[0] & 0xfc) >> 2;
            __char4[1] = ((__char3[0] & 0x03) << 4) +
            ((__char3[1] & 0xf0) >> 4);
            __char4[2] = ((__char3[1] & 0x0f) << 2) +
            ((__char3[2] & 0xc0) >> 6);
            __char4[3] = __char3[2] & 0x3f;

            for(__i = 0; (__i <4) ; __i++)
                __encoded += __alphabet[__char4[__i]];
            __i = 0;
        }
    }
    if (__i) {
        for(__j = __i; __j < 3; __j++)
            __char3[__j] = '\0';
        __char4[0] = (__char3[0] & 0xfc) >> 2;
        __char4[1] = ((__char3[0] & 0x03) << 4) +
        ((__char3[1] & 0xf0) >> 4);
        __char4[2] = ((__char3[1] & 0x0f) << 2) +
        ((__char3[2] & 0xc0) >> 6);
        __char4[3] = __char3[2] & 0x3f;
        for (__j = 0; (__j < __i + 1); __j++)
            __encoded += __alphabet[__char4[__j]];
        while((__i++ < 3))
            __encoded += '=';
    }
    return __encoded;
}

vector<uint8_t> __base64::decode (string what) {
    size_t __length = what.size();
    size_t __i = 0;
    size_t __j = 0;
    size_t __in = 0;
    uint8_t __char4[4], __char3[3];
    std::string __decoded;
    while (__length-- && (what[__in] != '=') && __valid(what[__in])) {
        __char4[__i++] = what[__in];
        __in++;
        if (__i ==4) {
            for (__i = 0; __i <4; __i++)
                __char4[__i] = __alphabet.find(__char4[__i]);
            __char3[0] = (__char4[0] << 2) +
            ((__char4[1] & 0x30) >> 4);
            __char3[1] = ((__char4[1] & 0xf) << 4) +
            ((__char4[2] & 0x3c) >> 2);
            __char3[2] = ((__char4[2] & 0x3) << 6) +
            __char4[3];
            for (__i = 0; (__i < 3); __i++)
                __decoded += __char3[__i];
            __i = 0;
        }
    }
    if (__i) {
        for (__j = __i; __j <4; __j++)
            __char4[__j] = 0;
        for (__j = 0; __j <4; __j++)
            __char4[__j] = __alphabet.find(__char4[__j]);
        __char3[0] = (__char4[0] << 2) +
        ((__char4[1] & 0x30) >> 4);
        __char3[1] = ((__char4[1] & 0xf) << 4) +
        ((__char4[2] & 0x3c) >> 2);
        __char3[2] = ((__char4[2] & 0x3) << 6) +
        __char4[3];
        for (__j = 0; (__j < __i - 1); __j++)
            __decoded += __char3[__j];
    }
    auto __bytes = vector<uint8_t>(__decoded.size());
    for (size_t __byte = 0;
         __byte < __decoded.size();
         __byte++) {
        __bytes[__byte] = __decoded[__byte];
    }
    return __bytes;
}



//   ######      ###    ##     ## ########
//  ##    ##    ## ##   ###   ### ##
//  ##         ##   ##  #### #### ##
//  ##   #### ##     ## ## ### ## ######
//  ##    ##  ######### ##     ## ##
//  ##    ##  ##     ## ##     ## ##
//   ######   ##     ## ##     ## ########

void savegame(vector<double> *game, string *to) {
    cout << "[Seriailising " << game->size() <<
    " values to " << *to << "]\n";
    auto __bytes = __tobytearray(*game);
    string __temp = __base64::encode(__bytes);
    filebuf __buffer;
    __buffer.open(*to, ios::out | ios::trunc);
    if (__buffer.is_open()) {
        ostream __stream(&__buffer);
        __stream << __temp;
        cout << "[";
        cout << game->size() << " values had been "
        "successfully written to " << *to << "]\n";
    } else throw runtime_error("GAME: "
                               "Error occured when trying to open " + *to + "\n");
    __buffer.close();
    return;
}

vector<double>* loadgame(string *from) {
    cout << "[Deserialising values from " <<
    *from << "]\n";
    string __temp = "";
    filebuf __buffer;
    __buffer.open(*from, ios::in);
    if (__buffer.is_open()) {
        istream __stream(&__buffer);
        __stream >> __temp;
    } else throw runtime_error("GAME: "
                               "Error occured when trying to open " + *from + "\n");
    __buffer.close();
    auto __bytes = __base64::decode(__temp);
    static auto __game = __todoublearray(__bytes);
    cout << "[";
    cout << __game.size() << " values had been "
    "successfully read from " << *from << "]\n";
    return &__game;
}



//  ########  ########         ######        ## ##
//  ##     ## ##     ##       ##    ##      ##  ##
//  ##     ## ##     ##       ##           ##   ##
//  ##     ## ########         ######     ##    ##
//  ##     ## ##     ##             ##   ##     ##
//  ##     ## ##     ##       ##    ##  ##      ##
//  ########  ########         ######  ##       ########

void __savedb(map<string, cluster> *db, string *to) {
    cout << "[Started low-level database writing]\n";
    string __overall = "", __temp;
    size_t __usercounter = 0;
    for (auto __iterator = db->begin();
         __iterator != db->end();
         __iterator++, __usercounter++) {
        __overall += __iterator->first + '\n';
        auto __bytes = vector<uint8_t>(0);
        cout << "[Writing " << __iterator->second.size() <<
        " points for user [" << __usercounter << "]]\n";
        auto __points = *(__iterator->second.getpoints());
        for (size_t __index = 0;
             __index < __iterator->second.size();
             __index++) {
            auto __point =
                __tobytearray(__points[__index]);
            __bytes.insert(__bytes.end(),
                           __point.begin(),
                           __point.end());
        }
        __temp = __base64::encode(__bytes);
        __overall += __temp + '\n';
    }
    filebuf __buffer;
    __buffer.open(*to, ios::out | ios::trunc);
    if (__buffer.is_open()) {
        ostream __stream(&__buffer);
        __stream << __overall;
    } else throw runtime_error("GAME: "
                               "Error occured when trying to open " + *to + "\n");
    __buffer.close();
    cout << "[";
    cout << db->size() << " database items had been "
    "successfully written to " << *to << "]\n";
    return;
}

map<string, cluster>* __loaddb(string *from) {
    cout << "[Started low-level database reading]\n";
    static map<string, cluster> __db;
    filebuf __buffer;
    __buffer.open(*from, ios::in);
    size_t __usercounter = 0;
    if (__buffer.is_open()) {
        istream __stream(&__buffer);
        string __name, __temp;
        while (__stream.peek() != '\0' || __stream.peek() != EOF) {
            getline(__stream, __name);
            getline(__stream, __temp);
            if (__name.size() == 0 || __name.empty())
                break;
            else {
                auto __bytes = __base64::decode(__temp);
                auto __doubles = __todoublearray(__bytes);
                size_t __weight = __doubles.size() / configs::dimensions();
                auto __cluster = cluster(configs::dimensions());
                auto __point = vector<double>(configs::dimensions());
                for (size_t __index = 0;
                     __index < __weight;
                     __index++) {
                    __point.clear();
                    __point.resize(configs::dimensions());
                    copy(__doubles.begin() + __index * configs::dimensions(),
                         __doubles.begin() + (__index + 1) * configs::dimensions(),
                         __point.begin());
                    __cluster.add(__point);
                }
                cout << "[Read " << __cluster.size() <<
                " points for user [" << __usercounter << "]]\n";
                if (__cluster.size() > 0) {

                    cout << "[Recalculating reference...]\n";
                    __cluster.recalc();
                }
                __db.insert({__name, __cluster});
                __usercounter++;
            }
            __name.clear();
            __temp.clear();
        }
    } else throw runtime_error("GAME: "
                               "Error occured when trying to open " + *from + "\n");
    cout << "[";
    cout << __db.size() << " database items had been "
    "successfully read from " << *from << "]\n";
    return &__db;
}
