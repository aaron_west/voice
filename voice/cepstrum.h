#ifndef __voice__cepstrum__
#define __voice__cepstrum__

#include <vector>

using namespace std;

enum __melscapprox : unsigned short {
    decimal,
    natural
};

class __melscconv {
public:
    static double tomel(double value, __melscapprox approximation);
    static double frommel(double value, __melscapprox approximation);
};

class mfcc20 {
private:
    const unsigned int _filters = 20;
    const unsigned int _coefs = 13;
    double _low;
    double _high;
    unsigned int _samplfreq;
    unsigned int _size;
    vector<double> _boundaries;
    void _calcboundaries();
    vector<double> _filter(size_t filter, vector<double> *data);
    double _calcenergy(vector<double> *data);
    vector<double> _getMFCC(vector<double> *data);
public:
    mfcc20();
    inline void setlow(double frequency);
    inline void sethigh(double frequency);
    void init(unsigned int fftsize, unsigned int samplfreq);
    vector<double> process(vector<double> *data);
};


#endif /* defined(__voice__cepstrum__) */
