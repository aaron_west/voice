#include <cmath>
#include <numeric>
#include <stdexcept>

#include "signalproc.h"
#include "dataproc.h"

using namespace std;



//  ##    ## ########  ##     ## ##       ########
//  ###   ## ##     ## ###   ### ##            ##
//  ####  ## ##     ## #### #### ##           ##
//  ## ## ## ########  ## ### ## ##          ##
//  ##  #### ##   ##   ##     ## ##         ##
//  ##   ### ##    ##  ##     ## ##        ##
//  ##    ## ##     ## ##     ## ######## ########

void peaknorm (vector<double> &data) {
    if (data.empty() ||
        (data.size() == 0))
        throw logic_error("SIGN: "
        "The data container provided is empty.");
    double __maximum = 0.;
    size_t __counter = 0;
    for (; __counter < data.size();
         __counter++)
        if (abs(data[__counter]) >= __maximum)
            __maximum = abs(data[__counter]);
    if (__maximum < numeric_limits<double>::epsilon())
        throw runtime_error("SIGN: "
        "Maximum detected peak equals zero.");
    for (__counter = 0;
         __counter < data.size();
         __counter++)
        data[__counter] /= __maximum;
    return;
}



//  ##     ##    ###    ##     ## ##     ## ##      ##
//  ##     ##   ## ##   ###   ### ###   ### ##  ##  ##
//  ##     ##  ##   ##  #### #### #### #### ##  ##  ##
//  ######### ##     ## ## ### ## ## ### ## ##  ##  ##
//  ##     ## ######### ##     ## ##     ## ##  ##  ##
//  ##     ## ##     ## ##     ## ##     ## ##  ##  ##
//  ##     ## ##     ## ##     ## ##     ##  ###  ###

void hammingwindow(vector<double> &data) {
    if (data.empty() ||
        (data.size() == 0))
        throw logic_error("SIGN: "
        "The data container provided is empty.");
    double __scaling = 0.;
    for (size_t __counter = 0;
         __counter < data.size();
         __counter++) {
        double __argument = 2 * pi * __counter /
            (data.size() - 1);
        __argument = cos(__argument);
        __scaling = 0.54 - 0.46 * __argument;
        data[__counter] *= __scaling;
    }
    return;
}



//  ######## ######## ########
//  ##       ##          ##
//  ##       ##          ##
//  ######   ######      ##
//  ##       ##          ##
//  ##       ##          ##
//  ##       ##          ##

vector<complex<double>>
fft::applyfft(vector<double> *data,
              size_t fftsize) {
    if (data == nullptr)
        throw invalid_argument("SIGN: "
        "Pointer provided is invalid.");
    if (data->empty() ||
        (data->size() == 0))
        throw logic_error("SIGN: "
        "The data container provided is empty.");
    if ((fftsize & (fftsize - 1)) != 0)
        throw invalid_argument("SIGN: "
        "FFT size provided does not match size rules.");
    auto __data = fillblock(data, fftsize);
    auto __processed =
        vector<complex<double>>(__data.size());
    for (size_t __counter = 0;
         __counter < __processed.size();
         __counter++) {
        __processed[__counter].real(__data[__counter]);
        __processed[__counter].imag(0.);
    }
    auto __temporary =
        valarray<complex<double>>(__processed.data(),
                                  __processed.size());
    _recursive(__temporary);
    __processed.assign(begin(__temporary),
                       end(__temporary));
    for (size_t __counter = 0;
         __counter < __processed.size();
         __counter++) {
        __processed[__counter].real(
            __processed[__counter].real() /
            sqrt(__data.size()));
        __processed[__counter].imag(
            __processed[__counter].imag() /
            sqrt(__data.size()));
    }
    return __processed;
}

void fft::_recursive(valarray<complex<double>> &data) {
    if (data.size() <= 1)
        return;
    size_t __size = data.size();
    valarray<complex<double>> __even =
        data[std::slice(0, __size >> 1, 2)];
    valarray<complex<double>> __odd =
        data[std::slice(1, __size >> 1, 2)];
    _recursive(__even);
    _recursive(__odd);
    for (size_t __counter = 0;
         __counter < (__size >> 1);
         __counter++) {
        double __temp = -2 * pi * __counter / __size;
        complex<double> temporary =
            std::polar(1., __temp) * __odd[__counter];
        data[__counter] =
            __even[__counter] + temporary;
        data[__counter + (__size >> 1)] =
            __even[__counter] - temporary;
    }
}

vector<double> fft::magnitudes(vector<complex<double>> *data) {
    if (data == nullptr)
        throw invalid_argument("SIGN: "
        "Pointer provided is invalid.");
    if (data->empty() ||
        (data->size() == 0))
        throw logic_error("SIGN: "
        "The data container provided is empty.");
    double __absolute = 0.0;
    auto __magnitudes = vector<double>(
        (*data).size() >> 1);
    double __temp1 = 0., __temp2 = 0.;
    for (size_t __counter = 0;
         __counter < ((*data).size() >> 1);
         __counter++) {
        __temp1 = pow((*data)[__counter].real(), 2);
        __temp2 = pow((*data)[__counter].imag(), 2);
        __absolute = sqrt(__temp1 + __temp2);
        __magnitudes[__counter] = __absolute;
    }
    return __magnitudes;
}
