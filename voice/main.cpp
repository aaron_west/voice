#include <iostream>
#include "interface.h"

int main(int argc, const char * argv[])
{
    interface::launch(argc, argv);
    interface::quit();
    return 0;
}

