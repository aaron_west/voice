#ifndef __voice__signalproc__
#define __voice__signalproc__

#include <iostream>
#include <vector>
#include <valarray>
#include <complex>

using namespace std;

const double pi = 3.141592653589793238462;

void peaknorm(vector<double> &data);

void hammingwindow(vector<double> &data);

class fft {
private:
    static void
    _recursive(valarray<complex<double>> &data);
public:
    static vector<complex<double>>
    applyfft(vector<double> *data,
             size_t fftsize);
    static vector<double>
    magnitudes(vector<complex<double>> *data);
};


#endif /* defined(__voice__signalproc__) */
