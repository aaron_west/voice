#ifndef __voice__logic__
#define __voice__logic__

#include <iostream>

#include "data.h"
#include "clustering.h"
#include "cepstrum.h"
#include "dataproc.h"
#include "configs.h"

class logic {
private:
    static framing _framing;
    static mfcc20 _mfcc;
    static kmeans _clustering;
    static bool _initialised;
public:
    static bool initialised();
    static void initialise();
    static void load(string samples_path);
    static void process(string samples_path,
                        string mfcc_path);
    static void learn(string mfcc_path,
                      string username);
    static void identify(string mfcc_path);
    static void dumbness(string mfcc_path, string userindex);
};

#endif
