#include <iostream>
#include <fstream>
#include <iomanip>
#include <cerrno>
#include <cstring>

#include "configs.h"
#include "data.h"

void __mfccout(vector<double> *mfcc) {
    for (size_t __dim = 0;
         __dim < configs::dimensions();
         __dim++) {
        cout << dec << setfill(' ') << fixed <<
        setw(9) << setprecision(4) <<  (*mfcc)[__dim] << "\t";
    }
    cout << "\n";
    return;
}

void __userout(size_t index,
               string username,
               vector<double> *mfcc) {
    cout << "[" << index << "]\t\t";
    cout << username << "\n";
    cout << "\t\t[Reference]\n\t\t";
    __mfccout(mfcc);
}

void __useroutfull(size_t index,
                   string username,
                   cluster *record) {
    cout << "[" <<
    index << "]\t\t";
    cout << username << "\n";
    cout << "\t\t[Reference]\n\t\t";
    __mfccout(record->getref());
    cout << "\t\t[Submitted MFCC vectors: "
        << record->size() << "]\n";
    auto __temp = *(record->getpoints());
    for (size_t __index = 0;
         __index < __temp.size();
         __index++) {
        cout << "\t\t";
        __mfccout(&__temp[__index]);
    }
}

size_t __calcindex(string username,
                   map<string, cluster> *db) {
    auto __iterator = db->find(username);
    size_t __index = 0;
    for (auto __running = db->begin();
         __running != __iterator;
         __running++)
        __index++;
    return __index;
}

bool data::_loaded = false;
map<string, cluster> data::database = map<string, cluster>();
storage<double> data::samples = storage<double>();
storage<double> data::mfcc = storage<double>();

bool data::loaded() {
    return _loaded;
}

void data::loaddb(string fromfile) {
    cout << "[Loading database from " <<
        fromfile << "]\n";
    if (exists(&fromfile)) {
        database = *(__loaddb(&fromfile));
        _loaded = true;
    }
    else {
        cout << "Database binary file "
        "was not found on your computer.\n";
        createdb(fromfile);
    }
    return;
}

void data::savedb(string tofile) {
    cout << "[Saving database to " <<
    tofile << "]\n";
    if (valid(&tofile))
        __savedb(&database, &tofile);
    else
        throw runtime_error("DATA: "
        "Path to binary file with database is "
        "not valid.");
    return;
}

void data::createdb(string infile) {
    std::cout << "Are you sure you want "
            "to create new database file? [Y/N]\n";
    char __YN = '\0';
    while (__YN != 'y') {
        std::cin.sync();
        std::cin >> __YN;
        switch (__YN) {
            case ('Y'):
            {
                cout << "Okay, let's do it...\n";
                __YN = 'y';
            }
                break;
            case ('N'):
            {
                cout << "Database creation "
                "cancelled by user.\n";
                return;
            }
            case ('y'):
                __YN = '\0';
            default:
            {
                cout << "Wrong option. "
                "Try again.\n";
                continue;
            }
        }
    }
    if (valid(&infile)) {
        cout << "[Creating database]\n";
        ofstream __stream(infile,
                          ios::out | ios::trunc);
        if (__stream.is_open()) {
            __stream << "Database binary file had "
            "been successfully created.";
            _loaded = true;
        }
        else {
            throw runtime_error("DATA: "
            "Failed to create database binary file " +
            infile + ": " + strerror(errno));
        }
        __stream.close();
        savedb(infile);
    }
    return;
}

void data::showdb(string fromfile) {
    loaddb(fromfile);
    cout << "[There are " << database.size() <<
        " records in the database]\n";
    size_t __index = 0;
    for (auto __iterator = database.begin();
         __iterator != database.end();
         __iterator++, __index++) {
        cout << "--------\n";
        __userout(__index,
                  __iterator->first,
                  __iterator->second.getref());
    }
    return;
}

void data::showdbfull(string fromfile) {
    loaddb(fromfile);
    cout << "[There are " << database.size() <<
    " records in the database]\n";
    size_t __index = 0;
    for (auto __iterator = database.begin();
         __iterator != database.end();
         __iterator++, __index++) {
        cout << "--------\n";
        __useroutfull(__index,
                      __iterator->first,
                      &__iterator->second);
    }
    return;
}

void data::adduser(string dbpath,
                   string username) {
    loaddb(dbpath);
    auto __iterator = database.find(username);
    if (__iterator == database.end()) {
        auto __mfcc =
            cluster(configs::dimensions());
        database.insert({username, __mfcc});
        cout << "[User " << username <<
            " had been added successfully]\n";
        savedb(dbpath);
        showuser(dbpath, username);
    } else throw runtime_error("DATA: "
           "User " + username + " already exists.");
    return;
}

void data::removeuser(string dbpath,
                      string username) {
    loaddb(dbpath);
    auto __iterator = database.find(username);
    if (__iterator == database.end())
        throw runtime_error("DATA: "
                            "User " + username + " was not found.");
    else {
        database.erase(__iterator);
        cout << "[User " << username <<
        " had been removed successfully]\n";
        savedb(dbpath);
    }
    return;
}

void data::showuser(string dbpath,
                    string username) {
    loaddb(dbpath);
    auto __iterator = database.find(username);
    if (__iterator == database.end())
        throw runtime_error("DATA: "
        "User " + username + " was not found.");
    else
        __userout(__calcindex(username, &database),
                  username,
                  __iterator->second.getref());
    return;
}

void data::showallforuser(string dbpath,
                          string username) {
    loaddb(dbpath);
    auto __iterator = database.find(username);
    if (__iterator == database.end())
        throw runtime_error("DATA: "
                            "User " + username + " was not found.");
    else
        __useroutfull(__calcindex(username, &database),
                      username,
                      &__iterator->second);
    return;
}
