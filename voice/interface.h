#ifndef __voice__interface__
#define __voice__interface__

#include <string>
#include <map>
#include <vector>

#include "data.h"
#include "logic.h"

class console {
public:
    static void log(std::string output);
    static void showdb(map<string, vector<double>> *db);
};

class interface {
private:
    enum _commands : int {
        test,
        load,
        process,
        learn,
        identify,
        show,
        manual,
        config,
        database,
        add,
        remove,
        all,
        random
    };
    enum _parameters : int {
        samplingfreq,
        fftsize,
        dimensions,
        dbpath
    };
    static map<string, int> _switches;
    static map<string, int> _parameternames;
public:
    static void launch(int argc, const char * argv[]);
    static void man();
    static void quit();
};

#endif /* defined(__voice__interface__) */
