#ifndef __voice__data__
#define __voice__data__

#include <map>
#include <string>

#include "storage.h"
#include "game.h"
#include "clustering.h"

using namespace std;

void __mfccout(vector<double> *mfcc);

class data {
private:
    static bool _loaded;
public:
    static bool loaded();
    static map<string, cluster> database;
    static storage<double> samples;
    static storage<double> mfcc;
    static void loaddb(string fromfile);
    static void savedb(string tofile);
    static void createdb(string infile);
    static void showdb(string fromfile);
    static void showdbfull(string tofile);
    static void adduser(string dbpath,
                        string username);
    static void removeuser(string dbpath,
                           string username);
    static void showuser(string dpbath,
                         string username);
    static void showallforuser(string dbpath,
                               string username);
};

#endif /* defined(__voice__data__) */
